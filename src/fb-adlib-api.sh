#!/bin/bash

#https://developers.facebook.com/tools/explorer/
access_token=""

######################################################################## setup
fp="/home/sol-nhl/get/ceba-vis"
mkdir -p $fp; cd $fp
rm -rf $fp/html
rm -rf $fp/json
rm -rf $fp/imgs
mkdir html json imgs; cd -

#for orgn in liberalerna socialdemokraterna moderaterna vänsterpartiet sverigedemokraterna miljöpartiet kristdemokraterna centerpartiet; do 
args=$1
for orgn in $args; do 

orgd=$(tr -cd '\11\12\15\40-\176' <<<$orgn | grep -o '^..')
echo "org is $orgn ($orgd) ..."

# testing
#done

######################################################################## html
mkdir -p $fp/html/$orgd

results_counter=0
while [ $results_counter -lt 9 ]; do

    if [ $results_counter -eq 0 ]; then
    echo "initiating api calls ..."
    
    #curl -G \
    -d "search_terms='"$orgn"'" \
    -d "ad_type=POLITICAL_AND_ISSUE_ADS" \
    -d "ad_active_status=ALL" \
    -d "ad_reached_countries=['SE']" \
    -d "impression_condition=HAS_IMPRESSIONS_LIFETIME" \
    -d "publisher_platforms=['FACEBOOK', 'INSTAGRAM']" \
    -d "access_token=$access_token" \
    "https://graph.facebook.com/v4.0/ads_archive" | \
    python3 -m json.tool > $fp/html/$orgd/$results_counter.urls
    
    curl -G \
    -d "search_terms='"$orgn"'" \
    -d "ad_type=POLITICAL_AND_ISSUE_ADS" \
    -d "ad_reached_countries=['US']" \
    -d "access_token=$access_token" \
    "https://graph.facebook.com/v8.0/ads_archive" | \
    python3 -m json.tool > $fp/html/$orgd/$results_counter.urls
    
    fi
    #ad_type={ALL, POLITICAL_AND_ISSUE_ADS, HOUSING_ADS, NEWS_ADS, UNCATEGORIZED_ADS}
    #impression_condition={HAS_IMPRESSIONS_LIFETIME, HAS_IMPRESSIONS_YESTERDAY, HAS_IMPRESSIONS_LAST_7_DAYS, HAS_IMPRESSIONS_LAST_30_DAYS, HAS_IMPRESSIONS_LAST_90_DAYS}
    #publisher_platforms={FACEBOOK, INSTAGRAM, AUDIENCE_NETWORK, MESSENGER, WHATSAPP}

    if [ $(wc -l $fp/html/$orgd/$results_counter.urls | cut -f1 -d' ') -lt 15 ]; then
    echo "end of results... exiting"
    break
    fi

    while IFS=$'\t' read query_type url; do
        #download each url as html
        if [ $query_type == "page_id" ]; then
        page_id=$url 
        echo "$query_type: $url"
        elif [ $query_type == "ad_snapshot_url" ]; then
        echo "fetching html page ... $results_counter"
        curl -s "$url" > $fp/html/$orgd/$results_counter-$page_id-$(date +%s%N | cut -b1-13).html
        elif [ $query_type == "next" ]; then
        #let results_counter=results_counter+1
        results_counter=$(($results_counter + 1))
        echo "fetching next results: $results_counter"
        curl -s "$url" | python3 -m json.tool > $fp/html/$orgd/$results_counter.urls
        fi
    done <<<$(egrep 'page_id|ad_snapshot_url|next' $fp/html/$orgd/$results_counter.urls | tr -s ' ' | sed -e 's/": "/\t/g' -e 's/^ "//g' -e 's/",$//g')

done #while results

######################################################################## json
mkdir -p $fp/json/$orgd

for fn in $fp/html/$orgd/*.html; do
fb=${fn##*/}
fj=${fb%.*}

if false; then
#html changed, so skipping this
egrep -o '<script>require\("TimeSliceImpl"\)\.guard\(function\(\).*</script>' $fn | \
egrep -o ';require\("InitialJSLoader"\).handleServerJS\(.*\);}, "ServerJS define",' | \
sed -e 's/;require("InitialJSLoader").handleServerJS(//g' -e 's/);}, "ServerJS define",.*//g' | \
python3 -m json.tool > $fp/json/$orgd/$fj.json
fi

echo "$fp/json/$orgd/$fj.json"
done #for html

######################################################################## imgs
mkdir -p $fp/imgs/$orgd

#for url in $(egrep -h -o 'https...scon[^"]+' $fp/json/$orgd/*.json); do 
for url in $(egrep -h -o 'original_image_url...https.....scontent[^"]+' $fp/html/$orgd/*.html | cut -f3 -d'"' | sed -e 's/\\//g'); do 

fn=$(sed 's/?.\+//g' <<<$url)
fs=${fn##*/}
fs=$fp/imgs/$orgd/$(date +%s%N | cut -b1-13)-$fs
curl --silent "$url" > "$fs"

echo "$url" "$fs"
done #for imgs

######################################################################## done
done #for orgs
echo "all orgs processed ..."

#
for d in $(ls -1 ~/get/ceba-vis/html/); do echo $d; find ~/get/ceba-vis/html/$d -iname '*.html*' | wc; done
